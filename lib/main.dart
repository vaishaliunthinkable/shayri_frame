import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/painting.dart';

void main() => runApp(MyApp());


class MyApp extends StatefulWidget {
  /// This widget is the root of your application.
  @override
  MyAppState createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shayri',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: MyHomePage(title: 'Animated Text Kit'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:    Container(
        padding: EdgeInsets.only(left: 20,top: 20),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: new BoxDecoration(
        image: new DecorationImage(
        image: new AssetImage('images/bird2.gif'),
    fit: BoxFit.cover,
    ),
        ),

        child: TyperAnimatedTextKit(
          speed:  Duration(milliseconds: 100),
          isRepeatingAnimation: false,
          onTap: () {
            print("Tap Event");
          },
          text: [
            "O re panchi!!\nTujhe pinjare se nikalna h..\nApne lie,\napni jindagi me aage bhadna h..\ndusro kelie,\nEk mishaal banana h..\nDuniya ki!!\nBheed me khud ko chamkana ..\nO re panchi!!\nTujhe pinjare se nikalna h..."
                "\n\n\n\n\n\t\t\t\t\t\t\t\t\t\t\t\t-Vaishali Chaudhary",
          ],
          textStyle: TextStyle(fontSize: 20.0,fontFamily:"Hanalei",fontStyle: FontStyle.normal,color: Colors.green[900],fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}